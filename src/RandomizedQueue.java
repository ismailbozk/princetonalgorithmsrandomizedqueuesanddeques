import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by bozkurti on 28/06/15.
 */
public class RandomizedQueue<Item> implements Iterable<Item> {
    private int lastIndex = 0;
    private Item[] queue;

    public RandomizedQueue() {                 // construct an empty randomized queue
        queue = (Item[]) new Object[4];//initial array
    }

    //region Iterator

    @Override
    public Iterator<Item> iterator() {  // return an iterator over items in order from front to end
        return new ListIterator();
    }

    private class ListIterator implements Iterator<Item>{
        private int currentIndex = 0;

        public ListIterator(){
            for (int i = 0; i < lastIndex; i++){
                int rndIndex = StdRandom.uniform(i + 1);
                Item temp = queue[i];
                queue[i] = queue[rndIndex];
                queue[rndIndex] = temp;
            }
        }

        //warning check here
        public boolean hasNext(){
            return (queue.length - 1 > currentIndex && queue[currentIndex+1] != null);
        }

        public void remove(){
            //not implemented.
            throw new UnsupportedOperationException();
        }

        public Item next(){
            if (queue.length <= currentIndex || queue[currentIndex] == null){ throw new NoSuchElementException();}
            Item item = queue[currentIndex];
            currentIndex++;
            return item;
        }
    }

    //endregion

    public boolean isEmpty(){                 // is the queue empty?
        return (lastIndex == 0);
    }

    public int size(){                        // return the number of items on the queue
        return this.lastIndex;
    }

    private void validateInputItem(Item item){
        if (item == null) { throw new NullPointerException(); }
    }

    public void enqueue(Item item) {           // add the item
        validateInputItem(item);

        if (queue.length == lastIndex){
            resizeQueue(queue.length * 2);
        }

        queue[lastIndex++] = item;
    }

    private void validateQueue(){
        if (this.lastIndex == 0) { throw new NoSuchElementException();}
    }

    public Item dequeue(){                    // remove and return a random item
        validateQueue();

        int randomIndex = StdRandom.uniform(lastIndex);
        Item temp = this.queue[randomIndex];
        this.queue[randomIndex] = this.queue[lastIndex - 1];
        this.queue[--lastIndex] = null;

        if (lastIndex > 4 && lastIndex == queue.length/4) {
            resizeQueue(queue.length/2);
        }

        return temp;
    }

    public Item sample(){                     // return (but do not remove) a random item
        validateQueue();

        int randomIndex = StdRandom.uniform(lastIndex);
        return this.queue[randomIndex];
    }

    private void resizeQueue(int capacity){
        Item [] temp = (Item[]) new Object[capacity];
        for (int i = 0; i < lastIndex; i++) {
            temp[i] = queue[i];
        }
        queue = temp;
    }

    public static void main(String[] args){   // unit testing
        RandomizedQueue<Integer> rndq = new RandomizedQueue();

        rndq.enqueue(0);
        rndq.enqueue(1);
        rndq.enqueue(2);
        rndq.enqueue(3);
        rndq.enqueue(4);
        rndq.enqueue(5);
        rndq.enqueue(6);

        Integer s = rndq.sample();
        s = rndq.sample();
        s = rndq.sample();
        s = rndq.sample();
        s = rndq.sample();

        Integer y = null;
        Iterator<Integer> ite = rndq.iterator();
        while (ite.hasNext()){
            y = ite.next();
        }


        s = rndq.dequeue();
        s = rndq.dequeue();
        s = rndq.dequeue();
        s = rndq.dequeue();
        s = rndq.dequeue();
        s = rndq.dequeue();


    }
}
