import java.util.Iterator;

/**
 * Created by bozkurti on 28/06/15.
 */
public class Subset {
    public static void main(String[] args){
        int k = Integer.parseInt(args[0]);

        RandomizedQueue<String> rndq = new RandomizedQueue<String>();

//        for (int i = 0; i < inputs.length; i++){
//            rndq.enqueue(inputs[i]);
//        }

        while (!StdIn.isEmpty()) {
            rndq.enqueue(StdIn.readString());
        }

        Iterator<String> ite = rndq.iterator();
        for (int i = 0; i < k; i++){
            System.out.println(ite.next());
        }
    }

}
