import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by bozkurti on 28/06/15.
 */

public class Deque<Item> implements Iterable<Item> {
    private Node first = null;
    private Node last = null;
    private int size = 0;

    public Deque() {                          // construct an empty deque

    }

    //region Node

    private class Node{
        public Node prev;
        public Item item;
        public Node next;
    }

    //endregion

    //region Iterator

    @Override
    public Iterator<Item> iterator() {  // return an iterator over items in order from front to end
        return new ListIterator();
    }

    private class ListIterator implements Iterator<Item>{
        private Node current = first;

        //warning check here
        public boolean hasNext(){
            return current.next != null;
        }

        public void remove(){
            //not implemented.
            throw new UnsupportedOperationException();
        }

        public Item next(){
            if (current.item == null){ throw new NoSuchElementException();}
            Item item = current.item;
            current = current.next;
            return item;
        }
    }

    //endregion

    public boolean isEmpty(){                 // is the deque empty?
        return size == 0;
    }

    public int size(){                        // return the number of items on the deque
        return size;
    }

    private void validateInputItem(Item item){
        if (item == null) { throw new NullPointerException(); }
    }

    public void addFirst(Item item){          // add the item to the front
        validateInputItem(item);

        Node temp = first;

        first = new Node();
        first.item = item;
        first.next = temp;

        if (temp != null) {temp.prev = first;}

        size++;

        if (size == 1) { last = first; }
    }

    public void addLast(Item item){           // add the item to the end
        validateInputItem(item);

        Node temp = last;
        last = new Node();
        last.item = item;
        last.prev = temp;

        if (temp != null) {temp.next = last;}

        size++;

        if (size == 1) { first = last; }
    }

    private void validateRemoveItem(){
        if (size == 0) { throw new NoSuchElementException(); }
    }

    public Item removeFirst() {                // remove and return the item from the front
        validateRemoveItem();

        Node temp = first;
        first = temp.next;
        if (first != null) {first.prev = null;}
        temp.next = null;

        size--;

        if (size == 0){            last = null;        }

        return temp.item;
    }

    public Item removeLast(){                 // remove and return the item from the end
        validateRemoveItem();

        Node temp = last;
        last = temp.prev;
        if (last != null ) {last.next = null;}
        temp.prev = null;

        size--;

        if (size == 0){            first = null;        }

        return temp.item;
    }

    public static void main(String[] args){   // unit testing
        Deque<Integer> intDequ = new Deque<Integer>();

        intDequ.addFirst(2);
        intDequ.addFirst(1);
        intDequ.addFirst(0);
        intDequ.addLast(3);
        intDequ.addLast(4);
        intDequ.addLast(5);

        Iterator ite = intDequ.iterator();
        ite.hasNext();
        ite.next();

        intDequ.removeFirst();
        intDequ.removeFirst();
        intDequ.removeFirst();
        intDequ.removeFirst();
        intDequ.removeFirst();
        intDequ.removeLast();

    }
}
